# Copy/Writing Sample

## Anja Klapper

### What’s Here
* Brief A: instructions for brochure.
* Brief B: instructions for manual.
* Project A: brochure for eye health.
* Project A Documentation: documentation for brochure.
* Project B: manual for a home media centre. *Note: images are not included, they were drawn in once printed.*
* Project B Documentation: documentation for manual.
