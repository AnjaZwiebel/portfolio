# Portfolio

## Anja Klapper

###### "Think of the solution, not the problem." - *Terry Goodkind*

### What’s Here
* Algorithm Analysis Sample
    *  Specifications
    *  Project folder
* Copy/Writing Sample
    *  Briefs A & B
    *  Projects A & B
* Logo Design Sample
    *  Brief
    *  Product
    *  Documentation and Process
* Responsive Website Sample
    *  Brief
    *  Documentation
    *  Project folder