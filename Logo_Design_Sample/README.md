# Logo Design Sample

## Anja Klapper

### What’s Here
* Brief: instructions and goals.
* Final Product: logo, advertisement, information design, and packaging.
* Process and Documentation: written rationale, PowerPoint presentation rationale, and examples of process (sketches, moodboard, alternate versions).