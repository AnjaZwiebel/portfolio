/*
 * This program implements MinDistance and MinDistance2 Algorithms
 * and runs tests for basic operation, time efficiency and correctness.
 *
 * Younghyun Song (n9366610) and Anja Klapper (n7566719) May 2017
 */

#include <iostream>
#include <math.h>
#include <cstdlib> // rand()
#include <stdio.h> // for abs
#include <stdlib.h> // for abs
#include <time.h> // clock
#include <fstream> // output
#include <iomanip> // clock
#define INF INT_MAX

using namespace std;
const int N = 500;

int MinDistance(int* A, int n);
int CountBasicOperatorMinDistance(int* A, int n);
int MinDistance2(int* A, int n);
int CountBasicOperatorMinDistance2(int* A, int n);
int DisplayMinDistance(int* A, int n);
int DisplayMinDistance2(int* A, int n);
void TimeEfficiencyTest();
void BasicOperationTest();
void Correctness();

int basicCount = 0;
int basicCount2 = 0;
int arrayLength = 0;
int arrayLengthTime = 0;
int arrayIncrement = 50;
int arrayIncrementTime = 50;
ofstream excelBasicOutput;
ofstream excelTimeOutput;
clock_t start_clock1, end_clock1;
clock_t start_clock2, end_clock2;

int main(){
    // Perform Correctness Tests
    Correctness();

    // Create Basic Count File
    excelBasicOutput.open("BasicOutput.csv", ios::app); // opens in append mode
    excelBasicOutput << "Array Length, Basic Operation Count Algorithm 1, Basic Operation Count Algorithm 2" << endl;
    // Perform the Basic Operation Test N amount of times
    for(int i = 0; i < N; i++){
       BasicOperationTest();
       // Change the length of the array for the next test.
       arrayLength += arrayIncrement;
    }
    // Close excel file.
    excelBasicOutput.close();

    // Create Time Efficiency File
    excelTimeOutput.open("TimeOutput.csv", ios::app);
    excelTimeOutput << "Array Length, Execution Times (ms) Algorithm 1, Execution Times (ms) Algorithm 2" << endl;
    // Perform the Time Efficiency Test N amount of times
    for(int i = 0; i < N; i++){
        TimeEfficiencyTest();
        // Change the length of the array for the next test.
       arrayLengthTime += arrayIncrementTime;
    }
    // Close excel file.
    excelTimeOutput.close();

    return 0;
}
/* First Algorithm: MinDistance
 * Input: Array A[0..n-1]) of numbers
 * Output: Minimum distance between two of its elements
 */
int MinDistance(int* A, int n){ // where A is array of 0..n-1 length
    // dmin <- infintitySymbol
    int dmin = INF;
    // for i <- 0 to n - 1 do
    for(int i = 0; i < (n - 1); i++){
        // for j <- 0 to n - 1 do
        for(int j = 0; j < (n - 1); j++){
            // if i != j and |A[i] - A[j]| < dmin
            if(i != j){
               if(abs(A[i] - A[j]) < dmin){
                   // dmin <- |A[i] - A[j]|
                   dmin = abs(A[i] - A[j]);
               }
            }
        }
    }
    // return dmin
    return dmin;
}

/* Counts the Basic Operator for the first Algorithm: MinDistance
 * Keeps track of the basic operator
 */
int CountBasicOperatorMinDistance(int* A, int n){
    int dmin = INF;
    for(int i = 0; i < (n - 1); i++){
        for(int j = 0; j < (n - 1); j++){
            if(i != j){
                basicCount += 1;
                if(abs(A[i] - A[j]) < dmin){
                    dmin = abs(A[i] - A[j]);
                    basicCount += 1;
                }
            }
        }
    }
    return dmin;

}

/* Second Algorithm: MinDistance2
 * Input: An array A[0..n - 1] of numbers
 * Output: The minimum distance d between two of its elements
 */
int MinDistance2(int* A, int n){ // where A is array of 0..n-1
    // dmin <- infinitySymbol
    int dmin = INF;
    // for i <- 0 to n - 2 do
    for(int i = 0; i < n - 2; i++){
        // for j <- i + 1 to n - 1 do
        for(int j = i + 1; j < n - 1; j++){
            // temp <- |A[i] - A[j]|
            int temp = abs(A[i] - A[j]);
            // if temp < dmin
            if(temp < dmin){
                // dmin <- temp
                dmin = temp;
            }
        }
    }
    // return dmin
    return dmin;
}


/*
 * Counts the Basic Operator for the second Algorithm: MinDistance2
 */
int CountBasicOperatorMinDistance2(int* A, int n){
    int dmin = INF;
    for(int i = 0; i < n - 2; i++){
        for(int j = i + 1; j < n - 1; j++){
            int temp = abs(A[i] - A[j]);
            basicCount2 += 1;
            if(temp < dmin){
                basicCount2 += 1;
                dmin = temp;
            }
        }
    }
    return dmin;
}

/*
 * Prints out the found minimum distance
 */
int DisplayMinDistance(int* A, int n){
   int dmin = INF;
    for(int i = 0; i < (n - 1); i++){
        for(int j = 0; j < (n - 1); j++){
            if(i != j){
                basicCount += 1;
                if(abs(A[i] - A[j]) < dmin){
                    dmin = abs(A[i] - A[j]);
                    basicCount += 1;
                }
            }
        }
    }
    cout << " The minimum distance is: "<< dmin << endl;
    return dmin;
}

/*
 * Prints out the found minimum distance
 */
int DisplayMinDistance2(int* A, int n){
    int dmin = INF;
    for(int i = 0; i < n - 2; i++){
        for(int j = i + 1; j < n - 1; j++){
            int temp = abs(A[i] - A[j]);
            basicCount2 += 1;
            if(temp < dmin){
                basicCount2 += 1;
                dmin = temp;
            }
        }
    }
    cout << " The minimum distance is: "<< dmin << endl;
    return dmin;
}

void TimeEfficiencyTest(){
    // Create an array for the experiment. Both algorithms will use the same array data.
    int* A = new int[arrayLengthTime];
    // Populate the array with random numbers.
    for (int i = 0; i < arrayLengthTime; i++){
        A[i] = rand() % (N * 10);
    }
    // Start the first clock after the array has been populated to avoid unnecessary timings.
    start_clock1 = clock();
    // To get an average time for an array length, loop through n instances of the same length array.
    for (int i = 0; i < 20; i++){
        MinDistance(A, arrayLengthTime);
    }
    // Stop the first clock after the first algorithm.
    end_clock1 = clock();
    // Calculate the average time of first algorithm.
    double elapsed_time1 =(end_clock1 - start_clock1) / double(CLOCKS_PER_SEC) * 1000.0;
    cout << "Average time to search an array of length " << arrayLengthTime << " was "
          << fixed << setprecision(5)<< elapsed_time1 << " ms." << endl;
    // Start the second clock after the array has been populated to avoid unnecessary timings.
    start_clock2 = clock();
    // To get an average time for an array length, loop through n instances of the same length array.
    for (int i = 0; i < 20; i++){
        MinDistance2(A, arrayLengthTime);
    }
    // Stop the second clock after the first algorithm.
    end_clock2 = clock();
    // Calculate the average time of the second algorithm.
    double elapsed_time2 =(end_clock2 - start_clock2) / double(CLOCKS_PER_SEC) * 1000.0;
    cout << "Average time to search an array1 of length " << arrayLengthTime << " was "
         << fixed << setprecision(5) << elapsed_time2 << " ms." << endl;
    // Output the finall basic counts into the Excel file
    excelTimeOutput << arrayLengthTime << "," << elapsed_time1 <<","<< elapsed_time2 << endl;
    // Reset array for the next experiment.
    delete[] A;
}

void BasicOperationTest(){
    // Create an array for the experiment.
    int* A = new int[arrayLength];
    // Populate the array with random numbers.
    for (int i = 0; i < arrayLength; i++){
        A[i] = rand() % (N * 10);
    }
    // Call both algorithm methods here to ensure that they use the same array data.
    CountBasicOperatorMinDistance(A, arrayLength);
    CountBasicOperatorMinDistance2(A, arrayLength);

    // Output the finall basic counts into the Excel file
    excelBasicOutput << arrayLength << "," << basicCount <<","<< basicCount2 << endl;

    // Reset array and counters for the next experiment.
    delete[] A;
    basicCount = 0;
    basicCount2 = 0;
} // end BasicOperationTest

void Correctness(){
    // Create non-random arrays for the experiment
    int A[10] = {10, 20, 30, 40, 50, 60, 65, 70, 80, 90};
    int B[50] = {100, 200, 300, 400, 500, 550, 600, 700, 800, 900, 1000, 1100, 1200, 1300, 1400, 1500, 1600,
                1700, 1800, 1900, 2000, 2100, 2200, 2300, 2400, 2500, 2600, 2700, 2800, 2900, 3000, 3100,
                3200, 3300, 3400, 3500, 3600, 3700, 3800, 3900, 4000, 4100, 4200, 4300, 4400, 4500, 4600,
                4700, 4800, 4900};

    cout << " MinDistance Algorithm 1 Correctness Test" << endl;
    cout << "" << endl;
    cout << " For array A, expect: 5" << endl;
    cout << " For array B, expect: 50" << endl;
    cout << "" << endl;
    cout << " Array A" << endl;
    DisplayMinDistance(A, 10);
    cout << " Array B" << endl;
    DisplayMinDistance(B, 50);
    cout << "" << endl;
    cout << " MinDistance2 Algorithm 2 Correctness Test" << endl;
    cout << "" << endl;
    cout << " For array A, expect: 5" << endl;
    cout << " For array B, expect: 50" << endl;
    cout << "" << endl;
    cout << " Array A" << endl;
    DisplayMinDistance2(A, 10);
    cout << " Array B" << endl;
    DisplayMinDistance2(B, 50);
}
